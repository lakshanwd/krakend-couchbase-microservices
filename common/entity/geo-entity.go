package entity

// Location - holds geo data
type Location struct {
	Longitude float64 `json:"lon,omitempty"`
	Latitude  float64 `json:"lat,omitempty"`
	Key       string  `json:"key,omitempty"`
}
