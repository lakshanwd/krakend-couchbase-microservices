package entity

// Company - holds company data
type Company struct {
	RestaurantID   int    `json:"restaurant_id,omitempty"`
	RestaurantName string `json:"restaurant_name,omitempty"`
	CurrencyCode   string `json:"currency_code,omitempty"`
	BranchID       int    `json:"branch_id,omitempty"`
	BranchName     string `json:"branch_name,omitempty"`
	Key            string `json:"key,omitempty"`
}
