package db

import (
	"fmt"

	"github.com/couchbase/gocb"
)

// GetAuthenticatedConnection - returns an authenticated connection
func GetAuthenticatedConnection(host, username, password string) (clutserPtr *gocb.Cluster, err error) {
	if clutserPtr, err = gocb.Connect(fmt.Sprintf("couchbase://%s", host)); err == nil {
		clutserPtr.SetEnhancedErrors(true)
		err = clutserPtr.Authenticate(gocb.PasswordAuthenticator{
			Username: username,
			Password: password,
		})
	}
	return
}
