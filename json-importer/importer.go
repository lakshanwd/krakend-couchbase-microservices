package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"

	"github.com/couchbase/gocb"
	"gitlab.com/lakshanwd/krakend-couchbase-microservices/common/db"
	"gitlab.com/lakshanwd/krakend-couchbase-microservices/common/entity"
)

func main() {
	var err error
	defer func() {
		if err != nil {
			log.Fatal(err)
		}
	}()

	var file, username, password, host string
	flag.StringVar(&file, "json", "./restaurant_branches_sample.json", "json data")
	flag.StringVar(&username, "u", "Administrator", "username")
	flag.StringVar(&password, "pw", "dev@123", "password")
	flag.StringVar(&host, "h", "127.0.0.1", "host")
	flag.Parse()
	companies, geos := parseFile(file)

	// connect to couchbase
	clutserPtr, err := db.GetAuthenticatedConnection(host, username, password)
	if err != nil {
		return
	}
	defer clutserPtr.Close()

	// get cluster manager
	clusterManagerPtr := clutserPtr.Manager(username, password)

	// create company bucket
	if err := clusterManagerPtr.InsertBucket(&gocb.BucketSettings{
		FlushEnabled:  true,
		Name:          "company",
		Password:      "",
		Replicas:      0,
		Quota:         100,
		IndexReplicas: false,
		Type:          gocb.Couchbase,
	}); err != nil {
		log.Println("Unabled to insert bucket - ", err.Error())
	}

	// create geo bucket
	if err := clusterManagerPtr.InsertBucket(&gocb.BucketSettings{
		FlushEnabled:  true,
		Name:          "geo",
		Password:      "",
		Replicas:      0,
		Quota:         100,
		IndexReplicas: false,
		Type:          gocb.Couchbase,
	}); err != nil {
		log.Println("Unabled to insert bucket - ", err.Error())
	}

	// retrieve company bucker reference
	companyBucketPtr, err := clutserPtr.OpenBucket("company", "")
	if err != nil {
		return
	}
	defer companyBucketPtr.Close()

	// store companies
	for _, company := range companies {
		companyBucketPtr.Insert(company.Key, company, 0)
	}

	// retrieve geo bucket reference
	geoBucketPtr, err := clutserPtr.OpenBucket("geo", "")
	if err != nil {
		return
	}
	defer geoBucketPtr.Close()

	// store geo locations
	for _, geo := range geos {
		geoBucketPtr.Insert(geo.Key, geo, 0)
	}
}

// reads the json file and parse the data
func parseFile(path string) (companies []entity.Company, geo []entity.Location) {
	var err error
	defer func() {
		if err != nil {
			log.Fatal(err)
		}
	}()

	filePtr, err := os.Open(path)
	if err != nil {
		return
	}
	defer filePtr.Close()

	type jsonData struct {
		RestaurantID   int     `json:"restaurant_id,omitempty"`
		RestaurantName string  `json:"restaurant_name,omitempty"`
		CurrencyCode   string  `json:"currency_code,omitempty"`
		BranchID       int     `json:"branch_id,omitempty"`
		BranchName     string  `json:"branch_name,omitempty"`
		Longitude      float64 `json:"lon,omitempty"`
		Latitude       float64 `json:"lat,omitempty"`
	}

	var data map[string][]jsonData
	decorderPtr := json.NewDecoder(filePtr)
	if err = decorderPtr.Decode(&data); err != nil {
		return
	}

	for _, e := range data["items"] {
		key := fmt.Sprintf("%d-%d", e.RestaurantID, e.BranchID)
		companies = append(companies, entity.Company{
			BranchID:       e.BranchID,
			BranchName:     e.BranchName,
			CurrencyCode:   e.CurrencyCode,
			RestaurantID:   e.RestaurantID,
			RestaurantName: e.RestaurantName,
			Key:            key,
		})
		geo = append(geo, entity.Location{
			Latitude:  e.Latitude,
			Longitude: e.Longitude,
			Key:       key,
		})
	}
	return
}
