package main

import (
	"flag"
	"fmt"
	"math"
	"net/http"
	"sort"
	"strconv"

	"gitlab.com/lakshanwd/krakend-couchbase-microservices/common/db"
	"gitlab.com/lakshanwd/krakend-couchbase-microservices/common/entity"

	"github.com/couchbase/gocb"
	"github.com/gin-gonic/gin"
)

// holds credentials of coachbase server
var username, password, host string
var port int

func main() {
	flag.StringVar(&username, "u", "Administrator", "username")
	flag.StringVar(&password, "pw", "dev@123", "password")
	flag.StringVar(&host, "h", "127.0.0.1", "host")
	flag.IntVar(&port, "p", 8060, "port")
	flag.Parse()

	r := gin.Default()
	r.GET("/location", getGeoLocations)
	r.Run(fmt.Sprintf(":%d", port))
}

func getGeoLocations(ctx *gin.Context) {
	var err error
	defer func() {
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, err.Error())
		}
	}()

	// connect to couchbase
	clutserPtr, err := db.GetAuthenticatedConnection(host, username, password)
	if err != nil {
		return
	}
	defer clutserPtr.Close()

	// retrieve geo bucket reference
	geoBucketPtr, err := clutserPtr.OpenBucket("geo", "")
	if err != nil {
		return
	}
	defer geoBucketPtr.Close()

	if err = geoBucketPtr.Manager("", "").CreatePrimaryIndex("", true, false); err != nil {
		return
	}

	lon, err := strconv.ParseFloat(ctx.Query("lon"), 10)
	if err != nil {
		return
	}
	lat, err := strconv.ParseFloat(ctx.Query("lat"), 10)
	if err != nil {
		return
	}
	rad, err := strconv.ParseFloat(ctx.Query("rad"), 10)
	if err != nil {
		return
	}

	queryPtr := gocb.NewN1qlQuery("SELECT * FROM geo")
	queryResults, err := geoBucketPtr.ExecuteN1qlQuery(queryPtr, nil)
	if err != nil {
		return
	}
	defer queryResults.Close()

	var e map[string]map[string]interface{}
	type locationDistance struct {
		distance float64
		location entity.Location
	}
	var temp []locationDistance
	for queryResults.Next(&e) {
		if distance := calculateDistance(lat, lon, e["geo"]["lat"].(float64), e["geo"]["lon"].(float64)); distance < rad {
			temp = append(temp, locationDistance{
				distance: distance,
				location: entity.Location{
					Key:       e["geo"]["key"].(string),
					Latitude:  e["geo"]["lat"].(float64),
					Longitude: e["geo"]["lon"].(float64),
				},
			})
		}
	}

	sort.Slice(temp, func(i, j int) bool {
		return temp[i].distance < temp[j].distance
	})

	var result []entity.Location
	for _, e := range temp {
		result = append(result, e.location)
	}

	ctx.JSON(http.StatusOK, gin.H{
		"locations": result,
	})
}

func calculateDistance(lat1, lng1, lat2, lng2 float64) (dist float64) {
	radlat1 := float64(math.Pi * lat1 / 180)
	radlat2 := float64(math.Pi * lat2 / 180)

	theta := float64(lng1 - lng2)
	radtheta := float64(math.Pi * theta / 180)

	dist = math.Sin(radlat1)*math.Sin(radlat2) + math.Cos(radlat1)*math.Cos(radlat2)*math.Cos(radtheta)
	if dist > 1 {
		dist = 1
	}

	dist = math.Acos(dist)
	dist = dist * 180 / math.Pi
	dist = dist * 60 * 1.1515
	return
}
