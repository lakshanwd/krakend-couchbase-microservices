package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"strconv"
	"strings"

	"github.com/devopsfaith/krakend/config"
	"github.com/devopsfaith/krakend/logging"
	"github.com/devopsfaith/krakend/proxy"
	"github.com/devopsfaith/krakend/router"
	krakendgin "github.com/devopsfaith/krakend/router/gin"
	"github.com/gin-gonic/gin"
	"gitlab.com/lakshanwd/krakend-couchbase-microservices/common/entity"
)

var port int
var logLevel, configFile string
var debug bool

func main() {
	var err error
	defer func() {
		if err != nil {
			log.Fatal(err)
		}
	}()

	flag.IntVar(&port, "p", 8080, "Port of the service")
	flag.StringVar(&logLevel, "l", "ERROR", "Logging level")
	flag.BoolVar(&debug, "d", true, "Enable the debug")
	flag.StringVar(&configFile, "c", "./config.json", "Path to the configuration filename")
	flag.Parse()

	// serves /findNearbyRestaurants
	go fireUpServer()

	// parse config
	parser := config.NewParser()
	serviceConfig, err := parser.Parse(configFile)
	if err != nil {
		return
	}

	serviceConfig.Debug = serviceConfig.Debug || debug
	if port != 0 {
		serviceConfig.Port = port
	}

	logger, err := logging.NewLogger(logLevel, os.Stdout, "[KRAKEND]")
	if err != nil {
		return
	}

	routerFactory := krakendgin.NewFactory(krakendgin.Config{
		Engine:         gin.Default(),
		ProxyFactory:   customProxyFactory{logger, proxy.DefaultFactory(logger)},
		Logger:         logger,
		HandlerFactory: krakendgin.EndpointHandler,
		RunServer:      router.RunServer,
	})

	routerFactory.New().Run(serviceConfig)
}

// customProxyFactory adds a logging middleware wrapping the internal factory
type customProxyFactory struct {
	logger  logging.Logger
	factory proxy.Factory
}

// New implements the Factory interface
func (cf customProxyFactory) New(cfg *config.EndpointConfig) (p proxy.Proxy, err error) {
	p, err = cf.factory.New(cfg)
	if err == nil {
		p = proxy.NewLoggingMiddleware(cf.logger, cfg.Endpoint)(p)
	}
	return
}

// serves requests for /findNearbyRestaurants if request comes through port 8070
func fireUpServer() {
	r := gin.Default()
	r.GET("/findNearbyRestaurants", findNearbyRestaurants)
	r.Run(":8070")
}

// handler function for findNearbyRestaurants
func findNearbyRestaurants(ctx *gin.Context) {
	var err error
	results := make(gin.H)
	defer func() {
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, err.Error())
			return
		}
		ctx.JSON(http.StatusOK, results)
	}()

	//parsing query parameters
	lon, err := strconv.ParseFloat(ctx.Query("lon"), 10)
	if err != nil {
		return
	}
	lat, err := strconv.ParseFloat(ctx.Query("lat"), 10)
	if err != nil {
		return
	}
	rad, err := strconv.ParseFloat(ctx.Query("rad"), 10)
	if err != nil {
		return
	}

	//retrieve locations
	err = getLocations(lon, lat, rad, &results)
}

// retrieves locations by calling /locations
func getLocations(lon, lat, rad float64, results *gin.H) (err error) {

	// preparing request
	url := fmt.Sprintf("http://localhost:8060/location?lon=%f&lat=%f&rad=%f", lon, lat, rad)
	requestPtr, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return
	}
	responsePtr, err := http.DefaultClient.Do(requestPtr)
	if err != nil {
		return
	}
	defer responsePtr.Body.Close()

	// reading response and parse data
	data := make(map[string][]entity.Location)
	decorderPtr := json.NewDecoder(responsePtr.Body)
	decorderPtr.Decode(&data)

	// update response
	for key, value := range data {
		(*results)[key] = value
	}

	// getting list of keys for /companies
	var keys []string
	for _, val := range data["locations"] {
		keys = append(keys, val.Key)
	}
	if len(keys) > 0 {
		err = getCompanies(keys, results)
	}
	return
}

// retrieves respected companies by calling /companies
func getCompanies(keys []string, results *gin.H) (err error) {
	temp, err := json.Marshal(keys)
	if err != nil {
		return
	}
	// preparing request
	url := "http://localhost:8050/companies"
	payload := strings.NewReader(string(temp))
	requestPtr, err := http.NewRequest("POST", url, payload)
	if err != nil {
		return
	}
	requestPtr.Header.Add("Content-Type", "application/json")
	responsePtr, err := http.DefaultClient.Do(requestPtr)
	if err != nil {
		return
	}
	defer responsePtr.Body.Close()

	// reading response and parse data
	data := make(map[string][]entity.Company)
	decorderPtr := json.NewDecoder(responsePtr.Body)
	decorderPtr.Decode(&data)

	// update response
	for key, value := range data {
		(*results)[key] = value
	}
	return
}
