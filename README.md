## krakend-couchbase-microservices

### setup

install golang [properly][1]  
setup [$GOPATH][2]  
setup couchbase-server properly  
open terminal and enter folowing

```sh
# install dep
go get -u github.com/golang/dep/cmd/dep

# clone the repo in $GOPATH/src/gitlab.com/lakshanwd/krakend-couchbase-microservices
git clone https://gitlab.com/lakshanwd/krakend-couchbase-microservices.git $GOPATH/src/gitlab.com/lakshanwd/krakend-couchbase-microservices

# Go to $GOPATH/src/gitlab.com/lakshanwd/krakend-couchbase-microservices
cd $GOPATH/src/gitlab.com/lakshanwd/krakend-couchbase-microservices

# install dependancies
dep ensure
```
#### import data

export data to a json file of following format
```js
{
  "items": [
    {
      "restaurant_id": "<int>",
      "restaurant_name": "<string>",
      "currency_code": "<string>",
      "branch_id": "<int>",
      "branch_name": "<string>",
      "lon": "<float64>",
      "lat": "<float64>"
    },
    ...
}
```
```sh
cd $GOPATH/src/gitlab.com/lakshanwd/krakend-couchbase-microservices/json-importer
cp /path/to/json/file.json .
go run importer.go -json file.json -u administrator -pw s3cr3t -h 127.0.0.1
```

Open 3 separate console windows, provide valid couchbase server credentials and execute following
```sh
# window 1
cd $GOPATH/src/gitlab.com/lakshanwd/krakend-couchbase-microservices/company-micro-service
go run company.go -u administrator -pw s3cr3t -h 127.0.0.1

# window 2
cd $GOPATH/src/gitlab.com/lakshanwd/krakend-couchbase-microservices/geo-micro-service
go run geo.go -u administrator -pw s3cr3t -h 127.0.0.1

# window 3
cd $GOPATH/src/gitlab.com/lakshanwd/krakend-couchbase-microservices/krakend-gateway
go run app.go
```

### testing

- get company
```sh
# through krakend
curl -X GET http://localhost:8080/company/2170-3660
# direct
curl -X GET http://localhost:8050/company/2170-3660
```

- get companies
```sh
# through krakend
curl -X POST http://localhost:8080/companies -H 'Content-Type: application/json' -d '["2170-3660","1186-1404"]'
# direct
curl -X POST http://localhost:8050/companies -H 'Content-Type: application/json' -d '["2170-3660","1186-1404"]'
```

- get closest locations
```sh
# through krakend
curl -X GET 'http://localhost:8080/location?lon=46.68911967394354&lat=24.706418934830566&rad=5'
# direct
curl -X GET 'http://localhost:8060/location?lon=46.68911967394354&lat=24.706418934830566&rad=5'
```

- get closest locations and company data
```sh
# through krakend
curl -X GET 'http://localhost:8080/findNearbyRestaurants?lon=46.68911967394354&lat=24.706418934830566&rad=5'
```

[1]:https://golang.org/doc/install
[2]:https://github.com/golang/go/wiki/SettingGOPATH