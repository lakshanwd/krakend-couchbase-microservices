package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"net/http"

	"gitlab.com/lakshanwd/krakend-couchbase-microservices/common/db"

	"gitlab.com/lakshanwd/krakend-couchbase-microservices/common/entity"

	"github.com/couchbase/gocb"
	"github.com/gin-gonic/gin"
)

// holds credentials of coachbase server
var username, password, host string
var port int

func main() {
	flag.StringVar(&username, "u", "Administrator", "couchbase username")
	flag.StringVar(&password, "pw", "dev@123", "cauchbase password")
	flag.StringVar(&host, "h", "127.0.0.1", "couchbase host")
	flag.IntVar(&port, "p", 8050, "port of the microservice")
	flag.Parse()

	r := gin.Default()
	r.GET("/company/:id", getCompany)
	r.POST("/companies", getCompanies)
	r.Run(fmt.Sprintf(":%d", port))
}

func getCompany(ctx *gin.Context) {
	var err error
	defer func() {
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, err.Error())
		}
	}()

	// connect to couchbase
	clutserPtr, err := db.GetAuthenticatedConnection(host, username, password)
	if err != nil {
		return
	}
	defer clutserPtr.Close()

	// retrieve company bucket reference
	companyBucketPtr, err := clutserPtr.OpenBucket("company", "")
	if err != nil {
		return
	}
	defer companyBucketPtr.Close()

	var result entity.Company
	companyBucketPtr.Get(ctx.Param("id"), &result)
	ctx.JSON(http.StatusOK, gin.H{
		"company": result,
	})
}

func getCompanies(ctx *gin.Context) {
	var err error
	defer func() {
		if err != nil {
			ctx.JSON(http.StatusInternalServerError, err.Error())
		}
	}()

	var ids []string
	decorderPtr := json.NewDecoder(ctx.Request.Body)
	if err = decorderPtr.Decode(&ids); err != nil {
		return
	}

	// connect to couchbase
	clutserPtr, err := db.GetAuthenticatedConnection(host, username, password)
	if err != nil {
		return
	}
	defer clutserPtr.Close()

	// retrieve company bucket reference
	companyBucketPtr, err := clutserPtr.OpenBucket("company", "")
	if err != nil {
		return
	}
	defer companyBucketPtr.Close()
	companyBucketPtr.Manager("", "").CreatePrimaryIndex("", true, false)

	queryPtr := gocb.NewN1qlQuery("SELECT * FROM company WHERE `key` in $ids")

	queryResults, err := companyBucketPtr.ExecuteN1qlQuery(queryPtr, map[string]interface{}{"ids": ids})
	if err != nil {
		return
	}
	defer queryResults.Close()

	var companies []entity.Company
	var e map[string]map[string]interface{}
	for queryResults.Next(&e) {
		companies = append(companies, entity.Company{
			BranchID:       int(e["company"]["branch_id"].(float64)),
			RestaurantID:   int(e["company"]["restaurant_id"].(float64)),
			RestaurantName: e["company"]["restaurant_name"].(string),
			CurrencyCode:   e["company"]["currency_code"].(string),
			BranchName:     e["company"]["branch_name"].(string),
			Key:            e["company"]["key"].(string),
		})
	}

	ctx.JSON(http.StatusOK, gin.H{
		"companies": companies,
	})
}
